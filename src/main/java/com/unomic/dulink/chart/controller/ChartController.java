package com.unomic.dulink.chart.controller;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
/**
 * Handles requests for the application home page.
 */
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/")
@Controller
public class ChartController {

	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	final int shopId = 1;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private ChartService chartService; 
	
	
	@RequestMapping(value="index")
	public String alarmReport(){
		return "chart/index";
	};
	
	@RequestMapping(value="getJigList4Report")
	@ResponseBody
	public String getJigList4Report(ChartVo chartVo){
		String result = ""; 
		try {
			result = chartService.getJigList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};

	@RequestMapping(value="getDeviceList4Report")
	@ResponseBody
	public String getDeviceList4Report(ChartVo chartVo){
		String result = ""; 
		try {
			result = chartService.getDeviceList4Report(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAlarmData")
	@ResponseBody
	public String getAlarmData(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getAlarmData(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getAlarmForceData")
	@ResponseBody
	public String getAlarmForceData(){
		String result = "";
		try {
			result = chartService.getAlarmForceData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	@RequestMapping(value="getExceptionList")
	@ResponseBody
	public String getExceptionList(ChartVo chartVo){
		String result = "";
		try {
			result = chartService.getExceptionList(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	};
	
	
	
	//common func
	
	@RequestMapping(value="login")
	@ResponseBody 
	public String loginCnt(ChartVo chartVo){
		String str = ""; 
		try {
			str = chartService.login(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return str;
	}
	
	
	@RequestMapping(value="getStartTime")
	@ResponseBody
	public String getStartTime(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getStartTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	
	@RequestMapping(value="getComName")
	@ResponseBody 
	public String getComName(ChartVo chartVo){
		String str = "";
		try {
			str = chartService.getComName(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getBanner")
	@ResponseBody
	public ChartVo getBanner(ChartVo chartVo){
		try {
			chartVo = chartService.getBanner(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return chartVo;
	}
	
	@RequestMapping(value="setAlarmException")
	@ResponseBody
	public String setAlarmException(String val){
		String result="success";
		try {
			
			chartService.setAlarmException(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			result="Failed";
		}
		return result;
	}
	
	@RequestMapping(value="setAlarmForce")
	@ResponseBody
	public String setAlarmForce(String val){
		String result="success";
		try {
			
			chartService.setAlarmForce(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			result="Failed";
		}
		return result;
	}
	
	@RequestMapping(value="unsetAlarmException")
	@ResponseBody
	public String unsetAlarmException(String val){
		String result="success";
		try {
			chartService.unsetAlarmException(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			result="Failed";
		}
		return result;
	}
	
	@RequestMapping(value="unsetAlarmForce")
	@ResponseBody
	public String unsetAlarmForce(String val){
		String result="success";
		try {
			chartService.unsetAlarmForce(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			result="Failed";
		}
		return result;
	}
	
	@RequestMapping(value="setException")
	@ResponseBody
	public String setException(String val){
		String result="success";
		try {
			chartService.setException(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			result="Failed";
		}
		return result;
	}
	
	@RequestMapping(value="checkAlarmForce")
	@ResponseBody
	public String checkAlarmForce(String val){
		String result = "";
		try {
			result = chartService.checkAlarmForce(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block 
			e.printStackTrace();
		}
		return result;
	}
	
};

