
package com.unomic.dulink.chart.domain;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;




@Getter
@Setter
@ToString
public class ChartVo{
	String sDate;
	String eDate;
	String startDateTime;
	String endDateTime;
	String delayTimeSec;
	String alarmCode;
	String alarmMsg;
	
	//common
	Integer shopId;
	String dvcId;
	String jig;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
	
	Integer isAlarmWeb;
	Integer isPushMobile;
	String exist;
	String alarmExist;
	String exception;
	Integer exceptionType;
}
