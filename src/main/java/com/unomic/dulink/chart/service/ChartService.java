package com.unomic.dulink.chart.service;

import com.unomic.dulink.chart.domain.ChartVo;


public interface ChartService {
	public String getJigList4Report(ChartVo chartVo) throws Exception;
	public String getDeviceList4Report(ChartVo chartVo) throws Exception;
	public String getAlarmData(ChartVo chartVo) throws Exception;
	public String getAlarmForceData() throws Exception;
	
	//common func
	public String login(ChartVo chartVo) throws Exception;
	public String getStartTime(ChartVo chartVo) throws Exception;
	public String getComName(ChartVo chartVo) throws Exception;
	public ChartVo getBanner(ChartVo chartVo) throws Exception;
	public void setAlarmException(String val) throws Exception;
	public void setAlarmForce(String val) throws Exception;	
	public void unsetAlarmException(String val) throws Exception;
	public void unsetAlarmForce(String val) throws Exception;	
	public void setException(String val) throws Exception;
	public String getExceptionList(ChartVo chartVo) throws Exception;
	public String checkAlarmForce(String val) throws Exception;
	
};
