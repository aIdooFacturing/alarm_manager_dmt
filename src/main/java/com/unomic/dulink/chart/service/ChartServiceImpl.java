package com.unomic.dulink.chart.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.controller.ChartController;
import com.unomic.dulink.chart.domain.ChartVo;
 

@Service
@Repository
public class ChartServiceImpl extends SqlSessionDaoSupport implements ChartService{
	private final static String namespace= "com.unomic.dulink.chart.";
	private static final Logger logger = LoggerFactory.getLogger(ChartController.class);
	
	@Override
	public String getStartTime(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String rtn = (String) sql.selectOne(namespace + "getStartTime", chartVo);
		return rtn;
	}
	
	@Override
	public String getComName(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = URLEncoder.encode((String) sql.selectOne(namespace + "getComName", chartVo),"utf-8");
		return str;
	}
	
	
	@Override
	public ChartVo getBanner(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		chartVo = (ChartVo) sql.selectOne(namespace + "getBanner", chartVo);
		return chartVo;
	}

	@Override
	public String login(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		String str = "";
		
		int exist = (int) sql.selectOne(namespace + "login", chartVo);
		
		if(exist==0){
			str = "fail";
		}else{
			str = "success";
		};
		
		return str;
	};
	
	//common func
	
	
	@Override
	public String getJigList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >jigList = sql.selectList(namespace + "getJigList4Report", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < jigList.size(); i++){
			Map map = new HashMap();
			map.put("jig", URLEncoder.encode(jigList.get(i).getJig(), "utf-8"));
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("jigList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}
	
	@Override
	public String getDeviceList4Report(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo >dvcList = sql.selectList(namespace + "getDeviceList4Report", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < dvcList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(dvcList.get(i).getName(), "utf-8"));
			map.put("dvcId", URLEncoder.encode(dvcList.get(i).getDvcId(), "utf-8"));
			list.add(map);
		};
		
		Map dvcMap = new HashMap();
		dvcMap.put("dvcList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(dvcMap);
		
		return str;
	}

	@Override
	public String getAlarmData(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmList = sql.selectList(namespace + "getAlarmData", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < alarmList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(alarmList.get(i).getName(),"UTF-8"));
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("alarmCode", alarmList.get(i).getAlarmCode());
			map.put("alarmMsg",  URLEncoder.encode(alarmList.get(i).getAlarmMsg(),"UTF-8"));
			map.put("dvcId", alarmList.get(i).getDvcId());
			map.put("exist", alarmList.get(i).getExist());
			
			list.add(map);
		};
		
		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}
	
	@Override
	public String getAlarmForceData() throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmForceList = sql.selectList(namespace + "getAlarmForceData");
		
		List list = new ArrayList();
		for(int i = 0; i < alarmForceList.size(); i++){
			Map map = new HashMap();
			map.put("dvcId", alarmForceList.get(i).getDvcId());
			map.put("name", URLEncoder.encode(alarmForceList.get(i).getName(),"UTF-8"));
			map.put("alarmCode", alarmForceList.get(i).getAlarmCode());
			map.put("alarmMsg",  URLEncoder.encode(alarmForceList.get(i).getAlarmMsg(),"UTF-8"));
			
			list.add(map);
		};
		
		Map alarmForceMap = new HashMap();
		alarmForceMap.put("alarmForceList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmForceMap);

		return str;
	}
	
	@Override
	public String checkAlarmForce(String val) throws Exception {
		
		String result = "no";
		
		// 리스트에 전달받은 값 넣기 
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		//System.out.println(val);
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setAlarmCode(tempObj.get("code").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}
		
		// 강제 알람 테이블에 있는 데이터 알람 리스트에 넣기 
		List <ChartVo> alarmForceList = sql.selectList(namespace + "getAlarmForceData");
		
		// nsy
		for(int i = 0; i < list.size(); i++) {
			logger.info("checkAlarmForce_ i _" + i);
			for(int j = 0; j < alarmForceList.size(); j++){
				logger.info("checkAlarmForce_ j _" + j);
				if (list.get(i).getAlarmCode().equals(alarmForceList.get(j).getAlarmCode()) 
					&&	list.get(i).getDvcId().equals(alarmForceList.get(j).getDvcId()) ){
					result = "yes_" + alarmForceList.get(j).getDvcId() + "_" + alarmForceList.get(j).getName() + "_" + alarmForceList.get(j).getAlarmCode();
					return result;
				}
				// logger.info(list.get(i).getAlarmCode()  + " | " + alarmForceList.get(j).getAlarmCode());
				// logger.info(list.get(i).getDvcId()  + " | " + alarmForceList.get(j).getDvcId());
			}
		}
		
		return result;
				
		
		
//		List alarmList = new ArrayList();		
//		for(int i = 0; i < alarmForceList.size(); i++){
//			Map map = new HashMap();
//			map.put("dvcId", alarmForceList.get(i).getDvcId());
//			map.put("name", URLEncoder.encode(alarmForceList.get(i).getName(),"UTF-8"));
//			map.put("alarmCode", alarmForceList.get(i).getAlarmCode());
//			map.put("alarmMsg",  URLEncoder.encode(alarmForceList.get(i).getAlarmMsg(),"UTF-8"));
//			
//			alarmList.add(map);
//		};
		
//		Map alarmForceMap = new HashMap();
//		alarmForceMap.put("alarmForceList", list);
//		
//		String str = "";
//		ObjectMapper om = new ObjectMapper();
//		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmForceMap);

		//return str;
	}

	@Override
	public void setAlarmException(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		System.out.println(val);
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setAlarmCode(tempObj.get("code").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}
		String str = "";
		sql.insert(namespace+"setAlarmException", list);
	}
		
	@Override
	public void setAlarmForce(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		//System.out.println(val);
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");
		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setAlarmCode(tempObj.get("code").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());

			list.add(chartVo);
		}
		String str = "";
		sql.insert(namespace+"setAlarmForce", list);
	}

	@Override
	public void unsetAlarmException(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setAlarmCode(tempObj.get("code").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			// chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());

			list.add(chartVo);
		}
		sql.delete(namespace+"unsetAlarmException", list);
	}
		
	@Override
	public void unsetAlarmForce(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		JSONArray jsonArray = (JSONArray) jsonObj.get("val");

		List<ChartVo> list = new ArrayList<ChartVo>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject tempObj = (JSONObject) jsonArray.get(i);

			ChartVo chartVo = new ChartVo();
			chartVo.setAlarmCode(tempObj.get("code").toString());
			chartVo.setDvcId(tempObj.get("dvcId").toString());
			// chartVo.setDeliveryNo(tempObj.get("deliveryNo").toString());

			list.add(chartVo);
			
			// logger.info(chartVo.getAlarmCode() + " , " + chartVo.getDvcId());
			// logger.info(list.get(i).getAlarmCode() + " ,, " + list.get(i).getDvcId());			 
		}
		sql.delete(namespace+"unsetAlarmForce", list);
	}

	@Override
	public void setException(String val) throws Exception {
		SqlSession sql = getSqlSession();
		JSONParser jsonParser = new JSONParser();
		System.out.println(val);
		JSONObject jsonObj = (JSONObject) jsonParser.parse(val);
		
		int exceptionType = ((Long)jsonObj.get("exceptionTypeVal")).intValue();
		
		
		JSONArray updateAlarmJsonArray = (JSONArray) jsonObj.get("updateAlarmVal");
		if (updateAlarmJsonArray.size() > 0){
			for (int i = 0; i < updateAlarmJsonArray.size(); i++) {
				JSONObject tempObj = (JSONObject) updateAlarmJsonArray.get(i);
	
				ChartVo chartVo = new ChartVo();
				chartVo.setAlarmCode(tempObj.get("code").toString());
				chartVo.setDvcId(tempObj.get("dvcId").toString());
				if (exceptionType == 1){
					chartVo.setIsAlarmWeb(((Long)tempObj.get("isAlarmWeb")).intValue());
					chartVo.setIsPushMobile(((Long)tempObj.get("isPushMobile")).intValue());
				}
				sql.update(namespace+"updateAlarmException", chartVo);
			}
		}
		System.out.println("updateAlarmException compleated");
		
		JSONArray insertAlarmJsonArray = (JSONArray) jsonObj.get("insertAlarmVal");
		if (insertAlarmJsonArray.size() > 0){
			List<ChartVo> insertAlarmlist = new ArrayList<ChartVo>();
			for (int i = 0; i < insertAlarmJsonArray.size(); i++) {
				JSONObject tempObj = (JSONObject) insertAlarmJsonArray.get(i);
	
				ChartVo chartVo = new ChartVo();
				chartVo.setAlarmCode(tempObj.get("code").toString());
				chartVo.setDvcId(tempObj.get("dvcId").toString());
				if (exceptionType == 1){
					chartVo.setIsAlarmWeb(((Long)tempObj.get("isAlarmWeb")).intValue());
					chartVo.setIsPushMobile(((Long)tempObj.get("isPushMobile")).intValue());
				}
				insertAlarmlist.add(chartVo);
			} 
			sql.insert(namespace+"setAlarmException", insertAlarmlist);
		}
		System.out.println("setAlarmException compleated");
		
		JSONArray deleteAlarmJsonArray = (JSONArray) jsonObj.get("deleteAlarmVal");
		if (deleteAlarmJsonArray.size() > 0){
			List<ChartVo> deleteAlarmList = new ArrayList<ChartVo>();
			for (int i = 0; i < deleteAlarmJsonArray.size(); i++) {
				JSONObject tempObj = (JSONObject) deleteAlarmJsonArray.get(i);
	
				ChartVo chartVo = new ChartVo();
				chartVo.setAlarmCode(tempObj.get("code").toString());
				chartVo.setDvcId(tempObj.get("dvcId").toString());
				if (exceptionType == 1){
					chartVo.setIsAlarmWeb(((Long)tempObj.get("isAlarmWeb")).intValue());
					chartVo.setIsPushMobile(((Long)tempObj.get("isPushMobile")).intValue());
				}
	
				deleteAlarmList.add(chartVo);
			}
			sql.delete(namespace+"unsetAlarmException", deleteAlarmList);
		}
		System.out.println("unsetAlarmException compleated");
	}
	
	@Override
	public String getExceptionList(ChartVo chartVo) throws Exception {
		SqlSession sql = getSqlSession();
		List <ChartVo> alarmList = sql.selectList(namespace + "getExceptionList", chartVo);
		
		List list = new ArrayList();
		for(int i = 0; i < alarmList.size(); i++){
			Map map = new HashMap();
			map.put("name", URLEncoder.encode(alarmList.get(i).getName(),"UTF-8"));
			map.put("startDateTime", alarmList.get(i).getStartDateTime());
			map.put("endDateTime", alarmList.get(i).getEndDateTime());
			map.put("delayTimeSec", alarmList.get(i).getDelayTimeSec());
			map.put("alarmCode", alarmList.get(i).getAlarmCode());
			if (alarmList.get(i).getAlarmMsg() != null){
				map.put("alarmMsg",  URLEncoder.encode(alarmList.get(i).getAlarmMsg(),"UTF-8"));
			} else {
				map.put("alarmMsg",  URLEncoder.encode("ALARM MESSAGE NOT EXIST","UTF-8"));
			}
			map.put("dvcId", alarmList.get(i).getDvcId());
			map.put("exist", alarmList.get(i).getExist());
			map.put("alarmExist", alarmList.get(i).getAlarmExist());
			map.put("isAlarmWeb", alarmList.get(i).getIsAlarmWeb());
			map.put("isPushMobile", alarmList.get(i).getIsPushMobile());
			
			list.add(map);
		};
		
		Map alarmMap = new HashMap();
		alarmMap.put("alarmList", list);
		
		String str = "";
		ObjectMapper om = new ObjectMapper();
		str = om.defaultPrettyPrintingWriter().writeValueAsString(alarmMap);

		return str;
	}
};