<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.rtl.min.css">
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.silver.min.css">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
  	overflow:hidden;
}
input[type="checkbox"] {
	transform: scale(2)
}
</style> 
<script type="text/javascript">
	
	var shopId=1

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		setDate();
		getJig();
		getGroup("all");
		createNav("mainten_nav", 0);
		
		setEl();
		time();
		
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events": "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#checkException").css({
			"margin-right" : getElSize(100)
		})
		
		$(".selectArea").css({
			"border-radius" : getElSize(13)
		})
		
		$(".selectBox").css({
			"height" : getElSize(150),
			"width" : getElSize(100),
			"border-right" : getElSize(10)+'solid black'
		})
		
		$("#exceptionInclusion").css({
			"margin-left" : getElSize(100)
		})
		
		$("#alarmForce").css({
			"margin-left" : getElSize(50)
		})
		
		if(window.sessionStorage.getItem("level")==2){
			
		}else{
			$("#checkException").css({
				"display" : "none"
			})
		}
		
		/* if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : getElSize(80),
				"z-index" : 99999,
				"left" : getElSize(1300),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1550),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : getElSize(100),
				"z-index" : 99999,
				"left" : getElSize(1300),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1550),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		} */
		
		if(getParameterByName('lang')=='ko'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(85),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#dashBoard_title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 99999,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		} 
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getGroup(jig){
		var url = "${ctxPath}/getDeviceList4Report.do";
		var param = "shopId=" + shopId;
		
		if (jig != ""){
			param += "&jig="+jig;
		}
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dvcList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
						
					option += "<option value='" + data.dvcId + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";  
 
				});
				
				$("#group").html(option);
				
//				getAlarmData();
			}
		});
	};
	
	function getJig(){
		var url = "${ctxPath}/getJigList4Report.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.jigList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.jig + "'>" + decodeURIComponent(data.jig).replace(/\+/gi," ") + "</option>";  
				});
				
				$("#jig").html(option);
				
//				getAlarmData();
			}
		});
	};
	
	function changeJig(val) {
		getGroup(val)
	}
	
	function getAlarmForceData(){
		
		$('#svg_td').loading({
			message: '${loading}',
			theme: 'dark'
		});
		
		deleteAlarmForceList = [];
		
		 var url = "${ctxPath}/getAlarmForceData.do";
		 
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				
				$('#svg_td').loading('stop');
				
				var json = data.alarmForceList;
				
				var tr = "<thead><tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
						"<td>${alarm_force}</td>" +
						"<td>${device}</td>" +
						"<td>${alarm}${number}</td>" +
						"<td style='width: 50%'>${alarm}${content}</td>" +
						"</tr></thead><tbody>";
				
				$(json).each(function(idx, data){
					var alarm = decodeURIComponent(data.alarmMsg).replace(/\+/gi, " ");
					
					tr += "<tr id='"+ data.dvcId +"_"+data.alarmCode+"'><td><input type='checkbox' checked onclick='checkAlarmForce(this)' class='alarm_"+ data.dvcId +"_"+data.alarmCode+"'></td>" +
					"<td>" + decode(data.name) + "</td>" + 
					"<td>" + data.alarmCode + "</td>" +					
					"<td>" + alarm + "</td></tr>";					
				});
				
				tr += "</tobdy>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40)
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1400)
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1510));
				$("#wrapper div:last").css("overflow", "auto"); 
			}
		});
	}
	
	var className = "";
	var classFlag = true;
	var exceptionType = 0;
	
	function getAlarmData(){
		
		insertAlarmList=[];
		insertAlarmForceList=[];
		deleteAlarmList=[];
		
		
		selectedAlarmList=[];
		currentAlarmList=[];
		
		if( $("#alarmForce").is(":checked") == true ){	
			getAlarmForceData();
			return;
		}
		
		$('#svg_td').loading({
			message: '${loading}',
			theme: 'dark'
		});
		var url = "${ctxPath}/getAlarmData.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val() + " 23:59:59";
		
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + 
					"&dvcId=" + $("#group").val() +
					"&jig=" + $("#jig").val() +
					"&shopId=" + shopId;
		
		if($("#exceptionInclusion").prop("checked")){
			url = "${ctxPath}/getExceptionList.do";
			param += "&exceptionType=1";
			exceptionType = 1;
		}else{
			param += "&exception=" + 0;
			exceptionType = 0;
		}
		
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				check = false;
				$('#svg_td').loading('stop');
				var json = data.alarmList;
				
				if(window.sessionStorage.getItem("level")==2){
					
					
					var tr = "<thead><Tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
					"<td>${alarm_exception}</td>";
					if (exceptionType == 0) {
						tr += "<td>${alarm_force}</td>";	
					}
					tr += "<td>${device}</td>";
					if (exceptionType == 0) {
						tr += "<td>${alarm}<br>${start_time}</td>" + 
						"<td>${alarm}<br>${end_time}</td>" +
						"<td>${fixed_time}(${minute})</td>";
					} else {
						tr += "<td>${alarm}${number}</td>"
					}
					tr += "<td style='width: 40%'>${alarm}${content}</td>"
					if (exceptionType != 0) {
					tr += "<td>Web</td>" +
						  "<td>Mobile</td>"
					}
					tr += "</tr></thead><tbody>";
				
					csvOutput = "${device},${alarm}${start_time}, ${alarm}${end_time}, ${fixed_time}, ${alarm}${content}LINE";
					
					$(json).each(function(idx, data){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var endDateTime;
						
						if(data.endDateTime!=null){
							endDateTime = data.endDateTime.substr(0,19);
						}else{
							endDateTime = '${proceeding}'
						};
						var delayTimeSec = data.delayTimeSec;
						
						if((data.endDateTime == null || data.endDateTime=="") && (data.delayTimeSec == null || data.delayTimeSec=="")){
							endDateTime = "-";
							delayTimeSec = "${in_progress}";
						}else{
							delayTimeSec = Math.ceil(delayTimeSec/60)
						}
						
						var orgData = {};
						orgData.dvcId = data.dvcId
						orgData.code = data.alarmCode
						orgData.index = idx;
						orgData.isDelete = 0;
						orgData.isPushMobile = data.isPushMobile;
						orgData.isAlarmWeb = data.isAlarmWeb;
						selectedAlarmList.push(orgData);
						
						var compData = {};
						compData.dvcId = data.dvcId
						compData.code = data.alarmCode
						compData.index = idx;
						compData.isDelete = 0;
						compData.isPushMobile = data.isPushMobile;
						compData.isAlarmWeb = data.isAlarmWeb;
						currentAlarmList.push(compData);
						
						var alarm;
						
						alarm =  data.alarmCode + "  " + decodeURIComponent(data.alarmMsg).replace(/\+/gi, " ");
						tr += "<tr id='"+ data.dvcId +"_"+data.alarmCode+"' class='contentTr " + className + "'>"
						if(data.alarmExist==1){
							if (exceptionType == 0) {
								tr += "<td><input type='checkbox' checked onclick='checkAlarm(this)' class='alarm_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
							} else {
								tr += "<td><input type='checkbox' checked onclick='checkAlarmPush(this, "+idx+")' id='alarm_"+idx+"' class='alarm_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
							}
						} else {
							if (exceptionType == 0) {
								tr += "<td><input type='checkbox' onclick='checkAlarm(this)' class='alarm_"+ data.dvcId +"_"+data.alarmCode+"'></td>" + 
								      "<td><input type='checkbox' onclick='checkAlarm(this)' class='alarmf_"+ data.dvcId +"_"+data.alarmCode+"'></td>";
							} else {
								tr += "<td><input type='checkbox' onclick='checkAlarmPush(this, "+idx+")' id='alarm_"+idx+"' class='alarm_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
							}
						}
						tr += "<td>" + decode(data.name) + "</td>"
						if (exceptionType == 0) {
							tr += "<td>" + data.startDateTime.substr(0,19) + "</td>" + 
							"<td>" + endDateTime + "</td>" + 
							"<td>" + delayTimeSec + "</td>"
						} else {
							tr += "<td>" + data.alarmCode + "</td>";
						}
						tr += "<td>" + alarm + "</td>";
						if (exceptionType != 0) {
							if (data.isAlarmWeb==1){
								tr += "<td><input type='checkbox' checked onclick='checkAlarmWeb(this, "+idx+")' id='alarm_web_"+idx+"' class='alarm_web_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
								if (data.isPushMobile==1){
									tr += "<td><input type='checkbox' checked disabled onclick='checkPushMobile(this, "+idx+")' id='push_mobile_"+idx+"' class='push_mobile_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
								} else {
									tr += "<td><input type='checkbox' disabled onclick='checkPushMobile(this, "+idx+")' id='push_mobile_"+idx+"' class='push_mobile_"+ data.dvcId +"_"+data.alarmCode+"'></td>"						
								}
							} else {
								tr += "<td><input type='checkbox' onclick='checkAlarmWeb(this, "+idx+")' id='alarm_web_"+idx+"' class='alarm_web_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
								if (data.isPushMobile==1){
									tr += "<td><input type='checkbox' checked onclick='checkPushMobile(this, "+idx+")' id='push_mobile_"+idx+"' class='push_mobile_"+ data.dvcId +"_"+data.alarmCode+"'></td>"
								} else {
									tr += "<td><input type='checkbox' onclick='checkPushMobile(this, "+idx+")' id='push_mobile_"+idx+"' class='push_mobile_"+ data.dvcId +"_"+data.alarmCode+"'></td>"						
								}
							}
						}
						tr += "</tr>";
						
						csvOutput += data.name + "," + 
						data.startDateTime.substr(0,19) + "," + 
						endDateTime + "," + 
						Math.ceil(delayTimeSec/60) + "," + 
						alarm + "LINE";
						});
						
				}else{
					
					var tr = "<thead><Tr style='font-weight: bolder;background-color: rgb(34,34,34)' class='thead'>" + 
					"<td>${device}</td>" + 
					"<td>${alarm}<br>${start_time}</td>" + 
					"<td>${alarm}<br>${end_time}</td>" + 
					"<td>${fixed_time}(${minute})</td>" + 
					"<td style='width: 50%'>${alarm}${content}</td>" + 
					"</tr></thead><tbody>";
				
					csvOutput = "${alarm}${start_time}, ${alarm}${end_time}, ${fixed_time}, ${alarm}${content}LINE";
					$(json).each(function(idx, data){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var endDateTime;
						
						if(data.endDateTime!=null){
							endDateTime = data.endDateTime.substr(0,19);
						}else{
							endDateTime = '${proceeding}'
						};
						var delayTimeSec = data.delayTimeSec;
						
						if((data.endDateTime == null || data.endDateTime=="") && (data.delayTimeSec == null || data.delayTimeSec=="")){
							endDateTime = "-";
							delayTimeSec = "${in_progress}";
						}else{
							delayTimeSec = Math.ceil(delayTimeSec/60)
						}
						
						var alarm;
						
						alarm =  data.alarmCode + "  " + decodeURIComponent(data.alarmMsg).replace(/\+/gi, " ");
						if(data.exist==1){
							tr += "<tr id='"+ data.dvcId +"_"+data.alarmCode+"_"+data.exist+"' class='contentTr " + className + "'>" +
								"<td>" + decode(data.name) + "</td>" +
								"<td>" + data.startDateTime.substr(0,19) + "</td>" + 
								"<td>" + endDateTime + "</td>" + 
								"<td>" + delayTimeSec + "</td>" + 
								"<td>" + alarm + "</td>";
							"</tr>";
						}else{
							tr += "<tr id='"+ data.dvcId +"_"+data.alarmCode+"_"+data.exist+"' class='contentTr " + className + "'>" +
								"<td>" + decode(data.name) + "</td>" +
								"<td>" + data.startDateTime.substr(0,19) + "</td>" + 
								"<td>" + endDateTime + "</td>" + 
								"<td>" + delayTimeSec + "</td>" + 
								"<td>" + alarm + "</td>";
							"</tr>";
						}
						
						csvOutput += data.name + "," + 
						data.startDateTime.substr(0,19) + "," + 
						endDateTime + "," + 
						Math.ceil(delayTimeSec/60) + "," + 
						alarm + "LINE";
						});
					
				}
				
				tr += "</tbody>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40)
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1400)
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1510));
				$("#wrapper div:last").css("overflow", "auto")
			}
		});
	};
	
	var check = false;
	$(function(){
		
		/* $("#checkException").click(function(){
			if(!check){
				$("#checkException").css({
					"background" : "black",
					"color" : "white"
				})
				var nameField=0;
				 $('.alarmTable').find('tr').each(function(){
					 if(nameField==0){
						 $(this).find('td').eq(0).before('<td style="width : 7%">알람예외</td>');
						 nameField++;
					 }else{
						 var splitString = $(this).closest('tr').attr('id').split('_')
						 if(splitString[2]==1){
							 $(this).find('td').eq(0).before('<td style="width : 7%"><input type="checkbox" checked class="'+splitString[0]+'_'+splitString[1]+'" onclick="checkAlarm(this)"></td>');
						 }else{
							 $(this).find('td').eq(0).before('<td style="width : 7%"><input type="checkbox" class="'+splitString[0]+'_'+splitString[1]+'" onclick="checkAlarm(this)"></td>');
						 }
					 }
			     });
				 check=true;
			}else{
				
				$("#checkException").css({
					"background" : "#EEEEEE",
					"color" : "black"
				})
				
				 $('.alarmTable').find('tr').each(function(){
				        $(this).find('td').eq(0).remove();
			     });
				 check=false;
			}
			
		}) */
		
				
		$("#exceptionInclusion").click(function(){
			
			if( $("#exceptionInclusion").is(":checked") == true ){
				if( $("#alarmForce").is(":checked") == true ){
					$("#alarmForce").prop('checked',false);
				}	
			}
			
			getAlarmData();
		})
		
		$("#alarmForce").click(function(){
			
			if( $("#alarmForce").is(":checked") == true ){				
				if( $("#exceptionInclusion").is(":checked") == true ){
					$("#exceptionInclusion").prop('checked',false);
				}
				
				//exceptionType = 2;
				getAlarmForceData();
			} else{
				getAlarmData();
			}
		})
	})
	 
	var insertAlarmList=[];
	var insertAlarmForceList=[];
	var deleteAlarmList=[];
	
	function checkAlarm(e){
		var splitString = $(e).closest("tr").attr('id').split('_')
		
		console.log(splitString)
		
		var dvcId = splitString[0]
		var alarmCode = splitString[1]
		
		var data={};
		data.dvcId = dvcId;
		data.code = alarmCode;
		
		
		var splitTdString = $(e).attr('class').split('_');		
		var name = splitTdString[0];
		
		var opositeName = '';
		
		if(name == 'alarmf'){
			opositeName = 'alarm' + '_' + dvcId + '_' + alarmCode;
		}else {
			opositeName = 'alarmf' + '_' + dvcId + '_' + alarmCode;
		}
				
		if($(e).prop("checked")){			
			$("." + opositeName).prop("checked",false);
			$('.'+$(e).attr('class')).prop('checked', true);
			
			var exist=false;
			for(var i=0;i<deleteAlarmList.length;i++){
				if(deleteAlarmList[i].code == alarmCode && deleteAlarmList[i].dvcId == dvcId){
					exist=true
					deleteAlarmList.splice(i,1);
				}
			}
			
			if(exist){
				
			}else{
				if(name == 'alarm'){
					console.log('alarm');
					insertAlarmList.push(data);
					
					for(var i=0;i<insertAlarmForceList.length;i++){
						if(insertAlarmForceList[i].code == alarmCode && insertAlarmForceList[i].dvcId == dvcId){
							console.log("dvcId3 : " + dvcId);
							console.log("insertAlarmForceList[i].dvcId : " + insertAlarmForceList[i].dvcId);
							insertAlarmForceList.splice(i,1);
							exist=true;
						}
					}
					
				} else {	// name == 'alarmf'
					console.log('force');
					insertAlarmForceList.push(data);
				
					for(var i=0;i<insertAlarmList.length;i++){
						if(insertAlarmList[i].code == alarmCode && insertAlarmList[i].dvcId == dvcId){
							insertAlarmList.splice(i,1)
							exist=true;
						}
					}
				}
								
				console.log(insertAlarmList);
				console.log(insertAlarmForceList);
			}
			
		}else{
			$('.'+$(e).attr('class')).prop('checked', false);
			
			var exist=false;
			
			if(name == 'alarm'){
				for(var i=0;i<insertAlarmList.length;i++){
					if(insertAlarmList[i].code == alarmCode && insertAlarmList[i].dvcId == dvcId){
						insertAlarmList.splice(i,1)
						exist=true;
					}
				}
			} else {	// name == 'alarmf'
				for(var i=0;i<insertAlarmForceList.length;i++){
					if(insertAlarmForceList[i].code == alarmCode && insertAlarmForceList[i].dvcId == dvcId){
						insertAlarmForceList.splice(i,1)
						exist=true;
					}
				}
			}
			
			console.log(insertAlarmList);
			console.log(insertAlarmForceList);			
			
			if(exist){
				
			}else{
				deleteAlarmList.push(data)
			}
		}
		
	}
	
	function checkAlarmForce(e){
		
		var splitString = $(e).closest("tr").attr('id').split('_')
		
		console.log(splitString)
		
		var dvcId = splitString[0]
		var alarmCode = splitString[1]
		
		var data={};
		data.dvcId = dvcId;
		data.code = alarmCode;
		
		
		var splitTdString = $(e).attr('class').split('_');		
						
		if($(e).prop("checked")){			
			
			for(var i=0;i<deleteAlarmForceList.length;i++){
				if(deleteAlarmForceList[i].code == alarmCode && deleteAlarmForceList[i].dvcId == dvcId){
					exist=true
					deleteAlarmForceList.splice(i,1)
				}
			}
			
		} else{
			deleteAlarmForceList.push(data);
		}
		
		console.log(deleteAlarmForceList);
			
	}
	
	var selectedAlarmList=[];
	var currentAlarmList=[];
	
	function checkAlarmPush(e, i){
		var splitString = $(e).closest("tr").attr('id').split('_');
		
		console.log(splitString);
		
		var dvcId=splitString[0];
		var alarmCode=splitString[1];	

		if($(e).prop("checked")){
			$('.'+$(e).attr('class')).prop('checked', true);
			currentAlarmList[i].isDelete = 0;
			$("#alarm_web_"+i).prop('checked', true);
			$("#push_mobile_"+i).prop('checked', true);
		}else{
			$('.'+$(e).attr('class')).prop('checked', false);
			$("#push_mobile_"+i).prop('disabled', false);
			currentAlarmList[i].isDelete = 1;
		}
		
		console.log(currentAlarmList);
	}
	
	function checkAlarmWeb(e, i){
		if($(e).prop("checked")){
			$('.'+$(e).attr('class')).prop('checked', true);
			$("#push_mobile_"+i).prop('disabled', true);
			currentAlarmList[i].isAlarmWeb = 1;
		}else{
			$('.'+$(e).attr('class')).prop('checked', false);
			$("#push_mobile_"+i).prop('disabled', false);
			currentAlarmList[i].isAlarmWeb = 0;
		}
	}
	
	function checkPushMobile(e, i){
		if($(e).prop("checked")){
			$('.'+$(e).attr('class')).prop('checked', true);
			console.log("i : " + i)
			console.log(currentAlarmList[i])
			currentAlarmList[i].isPushMobile = 1;
		}else{
			$('.'+$(e).attr('class')).prop('checked', false);
			console.log("i : " + i)
			console.log(currentAlarmList[i])
			currentAlarmList[i].isPushMobile = 0;
			
		}
	}
	
	
	function makeAlarmList(){
		
		var insertAlarmList = [];
		var updateAlarmList = [];
		var deleteAlarmList = [];
		
		var rtnObj=new Object();
		
		for (var i=0; i<selectedAlarmList.length; i++){
			// delete
			if (exceptionType == 0){
				if (selectedAlarmList[i].isDelete == currentAlarmList[i].isDelete){
					continue;
				} 
				
				if (currentAlarmList[i].isDelete == 1){
					deleteAlarmList.push(currentAlarmList[i]);
				} else {
					insertAlarmList.push(currentAlarmList[i]);
				}	
			} else {
				if (selectedAlarmList[i].isDelete == currentAlarmList[i].isDelete &&
						selectedAlarmList[i].isAlarmWeb == currentAlarmList[i].isAlarmWeb &&
						selectedAlarmList[i].isPushMobile == currentAlarmList[i].isPushMobile){
					continue;
				}
				if (currentAlarmList[i].isDelete == 1){
					// delete
					deleteAlarmList.push(currentAlarmList[i]);
				} else {
					if (selectedAlarmList[i].isDelete == currentAlarmList[i].isDelete &&
							(selectedAlarmList[i].isAlarmWeb != currentAlarmList[i].isAlarmWeb ||
							selectedAlarmList[i].isPushMobile != currentAlarmList[i].isPushMobile)){
					// update
						/* if ($("#push_mobile_"+i).prop('disabled')){
							currentAlarmList[i].isPushMobile = 0;
						} */
						updateAlarmList.push(currentAlarmList[i]);
					} else {
					// insert
						insertAlarmList.push(currentAlarmList[i]);
					}
				}
			}
		}
		
		rtnObj.insertAlarmVal=insertAlarmList
		rtnObj.updateAlarmVal=updateAlarmList
		rtnObj.deleteAlarmVal=deleteAlarmList
		
		return rtnObj
	}
	
	var deleteAlarmForceList = [];
	
	function setExceptionList(){
		
		if(window.sessionStorage.getItem("level")==2){
			
			if( $("#exceptionInclusion").is(":checked") == true ){
				exceptionType = 1;
			} else if( $("#alarmForce").is(":checked") == true ){
				exceptionType = 2;
			} else {
				exceptionType = 0;
			}

			console.log("exceptionType: " + exceptionType);
			
			if (exceptionType == 0) {
				
				if(insertAlarmList.length == 0 && deleteAlarmList.length == 0 && insertAlarmForceList.length == 0){
					alert("${any_check}");
					return;
				}
				
				if(insertAlarmList.length>0){
					
					var a=new Object();
					a.val=insertAlarmList
					a.exceptionTypeVal = exceptionType
					
					var url = "${ctxPath}/setAlarmException.do";
					var param = "val=" + JSON.stringify(a);
					
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							
							if(deleteAlarmList.length>0){
								var a=new Object();
								a.val=deleteAlarmList
								a.exceptionTypeVal = exceptionType
								
								var url = "${ctxPath}/unsetAlarmException.do";
								var param = "val=" + JSON.stringify(a);
								
								$.ajax({
									url : url,
									data : param,
									type : "post",
									dataType : "json",
									success : function(data){
										getAlarmData();										
									}
								})
							}else{
								getAlarmData();
							}
							
						}
					})
				}else if(deleteAlarmList.length>0){
					
					var a=new Object();
					a.val=deleteAlarmList
					a.exceptionTypeVal = exceptionType
					
					var url = "${ctxPath}/unsetAlarmException.do";
					var param = "val=" + JSON.stringify(a);
					
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							getAlarmData();
						}
					})
				}
				
				// AlarmForce insert
				if(insertAlarmForceList.length > 0){
					
					var a = new Object();
					a.val = insertAlarmForceList;
					
					// 이미 같은 값이 등록되어있는지 확인하기
					// check already data is existed
					var url = "${ctxPath}/checkAlarmForce.do";
					var param = "val=" + JSON.stringify(a);
					
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							
							console.log("data2 : " + data);
							
							var splitData = data.split('_');
							
							var isOverlap = splitData[0];
							var dvcId = splitData[1];
							var dvcName = splitData[2];
							var alarmCode = splitData[3];
							
							
							console.log(isOverlap + ", " + dvcId + ", " + dvcName + ", " + alarmCode);

							
							if(isOverlap == 'yes'){
								
								alert("${alarm_force_exist}" + "\n\n${alarm_force_device} : " + dvcName + "\n${alarm_force_alarmcode} : " + alarmCode + "\n\n${alarm_force_checkbox}");
								$(".alarmf_" + dvcId + '_' + alarmCode).prop("checked",false);
								
								for(var i=0;i<insertAlarmForceList.length;i++){
									if(insertAlarmForceList[i].code == alarmCode && insertAlarmForceList[i].dvcId == dvcId){
										insertAlarmForceList.splice(i,1);
									}
								}
								
							}else if (isOverlap == 'no'){	// 중복된 값이 없을 때 
								
								var url = "${ctxPath}/setAlarmForce.do";
								var param = "val=" + JSON.stringify(a);
								
								$.ajax({
									url : url,
									data : param,
									type : "post",
									success : function(data){
										
										if( insertAlarmList.length>0 || deleteAlarmList.length>0){
											
										} else if( insertAlarmForceList.length > 0){
											$("#alarmForce").prop('checked',true);
											getAlarmForceData();
										}
									}
								})							
								
							}else{
								alert("${alarm_force_duplicate}");
							}
						}
					})
					
					//console.log(insertAlarmForceList);
				}
				
								
			} else if(exceptionType == 1) {	// 알람 예외 목록 클릭 
				
				
				var alarms = makeAlarmList();
				alarms.exceptionTypeVal = exceptionType
				
				if(alarms.insertAlarmVal.length==0 && 
						alarms.updateAlarmVal.length==0 && 
						alarms.deleteAlarmVal.length==0){
					alert("${any_check}")
				} else {
					var url = "${ctxPath}/setException.do";
					var param = "val=" + JSON.stringify(alarms);
					
					console.log(param)
					
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							console.log(data)
							getAlarmData();
						}
					})
				}
				
			} else if(exceptionType == 2){	// 강제 알람 클릭 
				
				if(deleteAlarmForceList.length > 0){
					console.log("deleteAlarmForceList");
					console.log(deleteAlarmForceList);
					
					var a = new Object();
					a.val = deleteAlarmForceList;
					
					var url = "${ctxPath}/unsetAlarmForce.do";
					var param = "val=" + JSON.stringify(a);
					
					$.ajax({
						url : url,
						data : param,
						type : "post",
						success : function(data){
							getAlarmForceData();
						}
					})
				}else{
					alert("${any_uncheck}");
				}
			}
			
		} else{
			alert("${wrong_way}")
		}
	}
		
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	
</script>


</head>
<!-- <body oncontextmenu="return false"> -->

<style>
.selectArea{
	display:inline-block;
	background : gray;
}
.selectBox{
	vertical-align : middle;
	border-right: 2px solid black;
	display : -webkit-inline-box;
	background: rebeccapurple;
}
</style>
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title_right"></div>
	
	<div id="dashBoard_title">${alarm_manage}</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="contentTable" style="width : 100%">
						<Tr>
							<td>
								<spring:message code="device"></spring:message>
								<select id="jig" onchange="changeJig(this.value)"></select>
								<select id="group"></select>
								<spring:message code="op_period"></spring:message>
								<input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
								<%-- <button> <spring:message code="excel"></spring:message></button> --%>
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getAlarmData()">
								<div style="display: inline"><input id="exceptionInclusion" type="checkbox"><label for="exceptionInclusion"><spring:message code="alarm_exception_list"></spring:message></label></div>
								<div style="display: inline"><input id="alarmForce" type="checkbox"><label for="alarmForce"><spring:message code="alarm_force"></spring:message></label></div>
								<button id="checkException"class="k-button" style="float: right;" onclick="getAlarmData()"><spring:message code="cancel"></spring:message></button>
								<button class="k-button" style="float: right; margin-right:0;" onclick="setExceptionList()"><spring:message code="save"></spring:message></button>
							</td>
						</Tr>
					</table>
					<div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	